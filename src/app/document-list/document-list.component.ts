import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import { first } from 'rxjs/operators';

import { selectAll, DocumentState } from '../document.reducer';
import { Observable } from 'rxjs';
import { Document } from '../document.model';
import { AddDocument, FetchDocuments, ClearDocuments } from '../document.actions';

import uuid from 'uuid/v4';

@Component({
  selector: 'app-document-list',
  templateUrl: './document-list.component.html',
  styleUrls: ['./document-list.component.css']
})
export class DocumentListComponent implements OnInit {

  public documentList$: Observable<Document[]>;

  constructor(private store: Store<DocumentState>) { }

  ngOnInit() {
    this.store.dispatch(new FetchDocuments());
    this.documentList$ = this.store
      .select(selectAll);
  }


}
