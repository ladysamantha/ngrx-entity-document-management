import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { DocumentActionTypes, FetchDocuments, FetchDocumentsSuccess, AddDocument, AddDocumentSuccess, ClearDocuments } from './document.actions';
import { mergeMap, delay, map } from 'rxjs/operators';
import { Document } from './document.model';


@Injectable()
export class DocumentEffects {

  @Effect()
  fetchDocuments$ = this.actions$.pipe(
    ofType<FetchDocuments>(DocumentActionTypes.FetchDocuments),
    mergeMap(() => {
      const documents = JSON.parse(localStorage.getItem('documents')) || [];
      return of(documents).pipe(
        delay(500),
        map((docs) => new FetchDocumentsSuccess({ documents: docs }))
      );
    })
  );

  @Effect()
  addDocument$ = this.actions$.pipe(
    ofType<AddDocument>(DocumentActionTypes.AddDocument),
    mergeMap((action: AddDocument) => {
      const { document } = action.payload;
      const { id, name } = document;
      const docs: Document[] = JSON.parse(localStorage.getItem('documents')) || [];
      if (docs.findIndex(doc => doc.id === document.id) === -1) {
        const documents = [...docs, { id, name, file: null }];
        localStorage.setItem('documents', JSON.stringify(documents));
      }
      return of([]).pipe(
        delay(500),
        map(() => new AddDocumentSuccess({ document }))
      );
    })
  );

  @Effect()
  clearDocuments$ = this.actions$.pipe(
    ofType<ClearDocuments>(DocumentActionTypes.ClearDocuments),
    mergeMap(() => {
      localStorage.removeItem('documents');
      return of(new FetchDocuments());
    }),
  );

  constructor(private actions$: Actions) {
  }
}
