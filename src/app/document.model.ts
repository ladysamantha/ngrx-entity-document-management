export interface Document {
  id: string;
  file: File;
  name: string;
}
