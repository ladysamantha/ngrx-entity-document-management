import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { Document } from './document.model';
import { DocumentActions, DocumentActionTypes } from './document.actions';
import { createFeatureSelector } from '@ngrx/store';

import uuid from 'uuid/v4';

export interface DocumentState extends EntityState<Document> {
  // additional entities state properties
}

export const adapter: EntityAdapter<Document> = createEntityAdapter<Document>({
  selectId: () => uuid(),
});

export const initialState: DocumentState = adapter.getInitialState({
  // additional entity state properties
});

export function reducer(
  state = initialState,
  action: DocumentActions
): DocumentState {
  switch (action.type) {
    case DocumentActionTypes.AddDocumentSuccess: {
      return adapter.addOne(action.payload.document, state);
    }

    case DocumentActionTypes.UpsertDocument: {
      return adapter.upsertOne(action.payload.document, state);
    }

    case DocumentActionTypes.AddDocuments: {
      return adapter.addMany(action.payload.documents, state);
    }

    case DocumentActionTypes.UpsertDocuments: {
      return adapter.upsertMany(action.payload.documents, state);
    }

    case DocumentActionTypes.UpdateDocument: {
      return adapter.updateOne(action.payload.document, state);
    }

    case DocumentActionTypes.UpdateDocuments: {
      return adapter.updateMany(action.payload.documents, state);
    }

    case DocumentActionTypes.DeleteDocument: {
      return adapter.removeOne(action.payload.id, state);
    }

    case DocumentActionTypes.DeleteDocuments: {
      return adapter.removeMany(action.payload.ids, state);
    }

    case DocumentActionTypes.LoadDocuments: {
      return adapter.addAll(action.payload.documents, state);
    }

    case DocumentActionTypes.ClearDocuments: {
      return adapter.removeAll(state);
    }

    case DocumentActionTypes.FetchDocumentsSuccess: {
      return adapter.addAll(action.payload.documents, state);
    }

    default: {
      return state;
    }
  }
}

export const selectDocumentState = createFeatureSelector<DocumentState>('document');

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = adapter.getSelectors(selectDocumentState);
