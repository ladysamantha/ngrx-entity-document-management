import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';

import { AppComponent } from './app.component';
import { reducers, metaReducers } from './reducers';
import { environment } from '../environments/environment';
import { AppEffects } from './app.effects';

import * as fromDocuments from './document.reducer';
import { DocumentListComponent } from './document-list/document-list.component';
import { DocumentItemComponent } from './document-item/document-item.component';
import { CommonModule } from '@angular/common';
import { DocumentEffects } from './document.effects';
import { DocumentUploadComponent } from './document-upload/document-upload.component';

@NgModule({
  declarations: [
    AppComponent,
    DocumentListComponent,
    DocumentItemComponent,
    DocumentUploadComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    StoreModule.forRoot({
      document: fromDocuments.reducer
    }),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    EffectsModule.forRoot([AppEffects, DocumentEffects])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
