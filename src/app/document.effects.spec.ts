import { TestBed, inject } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { DocumentEffects } from './document.effects';

describe('DocumentEffects', () => {
  let actions$: Observable<any>;
  let effects: DocumentEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        DocumentEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.get(DocumentEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
