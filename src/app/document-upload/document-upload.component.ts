import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AddDocument, ClearDocuments } from '../document.actions';

import uuid from 'uuid/v4';

@Component({
  selector: 'app-document-upload',
  templateUrl: './document-upload.component.html',
  styleUrls: ['./document-upload.component.css']
})
export class DocumentUploadComponent implements OnInit {

  constructor(private store: Store<any>) { }

  ngOnInit() {
  }

  addDocument(event: any): void {
    const files: FileList = event.target.files as FileList;
    const file = files.item(0);
    const { name } = file;
    this.store.dispatch(new AddDocument({
      document: {
        id: uuid(),
        file,
        name,
      }
    }));
  }

  clearDocuments(): void {
    this.store.dispatch(new ClearDocuments());
  }
}
